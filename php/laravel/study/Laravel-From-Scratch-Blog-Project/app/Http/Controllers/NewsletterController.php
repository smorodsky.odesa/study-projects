<?php

namespace App\Http\Controllers;

use App\Services\NewsletterInterface;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class NewsletterController extends Controller
{
    public function __invoke(NewsletterInterface $newsletter)
    {
        request()->validate([
            'email' => ['required', 'email']
        ]);

        //'email_address' => 'vladislav.smorodskyi@gmail.com',
        try {
            $newsletter->subscribe(request('email'));
        } catch (\Exception $e) {
            throw ValidationException::withMessages([
                'email' => 'Email couldnt be added to newsletter list'
            ]);
        }

        return redirect('/')->with('success', 'You are now signed up for our newsletter');

    }
}

@props(['name'])

@error($name)
    <a href="" class="text-red-500 text-xs mt-1">{{ $message }}</a>
@enderror

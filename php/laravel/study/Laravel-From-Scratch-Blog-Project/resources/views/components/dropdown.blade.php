@props(['trigger'])

<div x-data="{ show: false }" @click.away="show = false" class="relative">
    {{-- Trigger --}}
    <div @click="show = !show">
        {{ $trigger }}
    </div>

    {{-- Links --}}
    <div x-show="show" class="px-2 absolute z-50 bg-gray-100 mt-2 w-full rounded overflow-auto max-h-32" style="display: none">
        {{ $slot }}
    </div>
</div>

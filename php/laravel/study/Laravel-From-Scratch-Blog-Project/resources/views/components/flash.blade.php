@if(session()->has('success'))
    <div x-data="{ show:true }"
         x-init="setTimeout(() => show = false, 4000)"
         x-show="show"
         class="fixed bottom-3 right-0 bg-blue-500 text-white text-sm py-2 px-4 rounded-xl" un>
        <p>{{ session('success') }}</p>
    </div>
@endif

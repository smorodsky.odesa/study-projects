@auth
    <x-panel>
        <form method="POST" action="/posts/{{ $post->slug }}/comments">
            @csrf

            <header class="flex items-center">
                <img src="https://i.pravatar.cc/100?u={{ auth()->id() }}" alt="" width="40"
                     height="40" class="rounded-full">

                <h2 class="ml-4">Leave a comment</h2>
            </header>
            <x-form.field>
                <x-form.textarea name="body"></x-form.textarea>
                <x-form.error name="body"></x-form.error>
            </x-form.field>
            <div class="flex justify-end mt-6 pt-6 border-t border-t-gray-100">
                <x-form.submit-button>
                    Add Comment
                </x-form.submit-button>
            </div>
        </form>
    </x-panel>
@else
    <p class="font-semibold">
        <a href="/register" class="underline">Register</a>
        or <a href="/login" class="underline">Log in </a> to leave a comment
    </p>
@endauth

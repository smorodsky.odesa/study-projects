<x-layout>
    <section class="px-6 py-8">
        <main class="max-w-lg mx-auto mt-10">
            <x-panel>
                <h1 class="text-center font-bold text-xl">Log in</h1>
                <form action="/login" class="mt-10" method="POST">
                    @csrf

                    <x-form.field>
                        <x-form.input name="email" type="email" autocomplete="username"></x-form.input>
                        <x-form.error name="email"></x-form.error>
                    </x-form.field>
                    <x-form.field>
                        <x-form.input name="password" type="password" autocomplete="new-password"></x-form.input>
                        <x-form.error name="password"></x-form.error>
                    </x-form.field>
                    <x-form.field>
                        <x-form.submit-button>Log In</x-form.submit-button>
                    </x-form.field>
                </form>
            </x-panel>
        </main>
    </section>
</x-layout>
